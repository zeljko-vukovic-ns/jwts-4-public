var wafepaApp = angular.module('wafepaApp.services', []);

wafepaApp.service('activityRestService', function($http) {
	
	this.apiActivitiesUrl = 'api/activities/';
	
	this.getActivities = function(parameters) {
		return $http.get(this.apiActivitiesUrl, { params : parameters });
	};
	
	this.deleteActivity = function(id) {
		return $http.delete(this.apiActivitiesUrl + id);
	};
	
	this.saveActivity = function(activity) {
		if (activity.id) {
			return $http.put(this.apiActivitiesUrl + activity.id, activity);
		} else {
			return $http.post(this.apiActivitiesUrl, activity);
		}
	};
	
	this.getActivity = function(id) {
		return $http.get(this.apiActivitiesUrl + id);
	};
});