var wafepaApp = angular.module('wafepaApp', ['ngRoute', 'wafepaApp.directives', 'wafepaApp.controllers', 'wafepaApp.services']); // wafepaApp je AngularJS aplikacija (u index.html <html ng-app="wafepaApp">

// konfiguracija $routeProvider - servis koji omogucuje promenu stranica, tj. rutiranje
// 	- u zavisnosti od toga koja je ruta pozvana, renderovace se odgovarajuci parcijalni view (definisan u templateUrl)
//	  ununtar elementa koji je oznacen sa ng-view direktivom
wafepaApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : 'resources/html/home.html'
	    })
	    .when('/activities', {
	        templateUrl : 'resources/html/activities.html',
	        controller : 'ActivitiesController'
	    })
	    .when('/activities/add', {
	        templateUrl : 'resources/html/addEditActivity.html',
	        controller : 'ActivitiesController'
	    })
	    .when('/activities/edit/:id', {
	        templateUrl : 'resources/html/addEditActivity.html',
	        controller : 'ActivitiesController'
	    })
	    .otherwise({
            redirectTo: '/'
        });
}]);