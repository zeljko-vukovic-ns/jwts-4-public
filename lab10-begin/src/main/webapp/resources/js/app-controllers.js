var wafepaApp = angular.module('wafepaApp.controllers', []);

wafepaApp.controller('ActivitiesController', function($scope, $location, $routeParams, activityRestService) {
	
	$scope.page = 0;
	
	$scope.getActivities = function() {
		
		var parameters = { page : $scope.page };
		
		if ($scope.search) {
			//parameters = { name : $scope.search, page : $scope.page };
			parameters.name = $scope.search; 	// objekat parameters je vec napravljen,
												// pa mu samo dodajemo novi atribut
		}
		
		activityRestService.getActivities(parameters)	   // poziva se GET api/activities
			.success(function(data) {			// ako je odgovor 200 OK, u data se nalaze trazeni podaci
				$scope.activities = data;
			})
			.error(function() { 				// ako se dogodila greska
				$scope.errorMessage = 'Oops, something went wrong!';
			});
	};
	
	$scope.initActivity = function() {
		$scope.activity = {};
		
		if ($routeParams.id) {
			activityRestService.getActivity($routeParams.id)
				.success(function(data) {
					$scope.activity = data;
				});
		}
	};
	
	$scope.saveActivity = function() {
		activityRestService.saveActivity($scope.activity)
			.success(function(data) {
				$location.path('/activities');
			});
	};
	
	$scope.deleteActivity = function(id, index) {
		activityRestService.deleteActivity(id)
			.success(function(data) {
				$scope.activities.splice(index, 1);
			});
	};
	
	$scope.changePage = function(page) {
		$scope.page = page;
		$scope.getActivities();
	};
});