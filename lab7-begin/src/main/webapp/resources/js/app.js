var wafepaApp = angular.module('wafepaApp', ['ngRoute']); // wafepaApp je AngularJS aplikacija (u index.html <html ng-app="wafepaApp">)

wafepaApp.controller('ActivitiesController', function($scope, $http) {
	
	$http.get('api/activities') 			// poziva se GET api/activities
		.success(function(data) {			// ako je odgovor 200 OK, u data se nalaze trazeni podaci
			$scope.activities = data;
		})
		.error(function() { 				// ako se dogodila greska
			
		});
});

// konfiguracija $routeProvider - servis koji omogucuje promenu stranica, tj. rutiranje
// 	- u zavisnosti od toga koja je ruta pozvana, renderovace se odgovarajuci parcijalni view (definisan u templateUrl)
//	  ununtar elementa koji je oznacen sa ng-view direktivom
wafepaApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : 'resources/html/home.html'
	    })
	    .when('/activities', {
	        templateUrl : 'resources/html/activities.html',
	        controller : 'ActivitiesController'
	    })
	    .otherwise({
            redirectTo: '/'
        });
}]);