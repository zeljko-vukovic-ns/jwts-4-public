package jwts.wafepa.web.dto;

import jwts.wafepa.model.Activity;

public class ActivityDTO {
	
	private String name;
	private Long id;
	
	
	
	public ActivityDTO() {
		super();
	}

	public ActivityDTO(Activity activity){
		super();
		
		this.id = activity.getId();
		this.name = activity.getName();
	}

	public String getName() {
		return name;
	}

	public void setAName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

	
	
}
