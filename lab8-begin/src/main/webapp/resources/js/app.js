var wafepaApp = angular.module('wafepaApp', ['ngRoute']); // wafepaApp je AngularJS aplikacija (u index.html <html ng-app="wafepaApp">)

wafepaApp.controller('ActivitiesController', function($scope, $http, $location) {
	
	$scope.getActivities = function() {
		$http.get('api/activities') 			// poziva se GET api/activities
			.success(function(data) {			// ako je odgovor 200 OK, u data se nalaze trazeni podaci
				$scope.activities = data;
			})
			.error(function() { 				// ako se dogodila greska
				$scope.errorMessage = 'Oops, something went wrong!';
			});
	};
	
	$scope.initActivity = function() {
		$scope.activity = {};
	};
	
	$scope.saveActivity = function() {
		$http.post('api/activities', $scope.activity)
			.success(function(data) {
				$location.path('/activities');
			});
	};
	
	$scope.deleteActivity = function(id, index) {
		$http.delete('api/activities/' + id)
			.success(function(data) {
				$scope.activities.splice(index, 1);
			});
	};
});

// konfiguracija $routeProvider - servis koji omogucuje promenu stranica, tj. rutiranje
// 	- u zavisnosti od toga koja je ruta pozvana, renderovace se odgovarajuci parcijalni view (definisan u templateUrl)
//	  ununtar elementa koji je oznacen sa ng-view direktivom
wafepaApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : 'resources/html/home.html'
	    })
	    .when('/activities', {
	        templateUrl : 'resources/html/activities.html',
	        controller : 'ActivitiesController'
	    })
	    .when('/activities/add', {
	        templateUrl : 'resources/html/addEditActivity.html',
	        controller : 'ActivitiesController'
	    })
	    .otherwise({
            redirectTo: '/'
        });
}]);