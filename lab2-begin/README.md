## Lab 2 - Spring MVC, Dependency injection

### Spring MVC
![Spring MVC](https://gitlab.com/jocicmarko/jwts/raw/master/img/dispatcher.png)

* Dodati Spring dependency-je u POM projekta (pom.xml)

```xml
<dependency>
	<groupId>org.springframework</groupId>
	<artifactId>spring-context</artifactId>
	<version>4.0.1.RELEASE</version>
	<exclusions>
		<exclusion>
			<groupId>commons-logging</groupId>
			<artifactId>commons-logging</artifactId>
		</exclusion>
	</exclusions>
</dependency>

<dependency>
	<groupId>org.springframework</groupId>
	<artifactId>spring-webmvc</artifactId>
	<version>4.0.1.RELEASE</version>
</dependency>

<!-- Logging -->
<dependency>
	<groupId>org.slf4j</groupId>
	<artifactId>jcl-over-slf4j</artifactId>
	<version>1.7.6</version>
</dependency>
<dependency>
	<groupId>org.slf4j</groupId>
	<artifactId>slf4j-api</artifactId>
	<version>1.7.6</version>
</dependency>
```

* Napraviti Spring context fajl (src/main/resources/ctx/application-context.xml). Dodati bean i mvc namespace-ove.
* Konfigurisati Spring MVC kroz context fajl - dodati view resolver za JSP stranice i simple view controller (mapiranje za "/" na "home" view)

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:mvc="http://www.springframework.org/schema/mvc"
	xmlns:p="http://www.springframework.org/schema/p"
	xmlns:context="http://www.springframework.org/schema/context"
	xsi:schemaLocation="http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc-4.0.xsd
		http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
		http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.0.xsd">
		
	<bean class="org.springframework.web.servlet.view.InternalResourceViewResolver" 
		p:prefix="/WEB-INF/jsp/"
		p:suffix=".jsp" />
	
	<mvc:view-controller path="/" view-name="home"/>
</beans>
```

* Napraviti JSP home stranicu (u src/main/webapp/WEB-INF/jsp folderu). Izbrisati index.jsp stranicu.
* Konfigurisati učitavanje Spring context-a u web.xml
* Nadograditi web.xml na servlet specifikaciju 3.0
* Konfigurisati Spring MVC Dispatcher Servlet u web.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://java.sun.com/xml/ns/javaee" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd" version="3.0">
	
  <display-name>JWTS Lab2</display-name>
  
  <!-- Context Loader Listener -->
	<listener>
		<listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
	</listener>

	<!-- Context Configuration -->
	<context-param>
		<param-name>contextConfigLocation</param-name>
		<param-value>classpath:/ctx/application-context.xml</param-value>
	</context-param>

	<!-- Spring Dispatcher Servlet -->
	<servlet>
		<servlet-name>wafepa</servlet-name>
		<servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
		<init-param>
			<param-name>contextConfigLocation</param-name>
			<param-value></param-value>
		</init-param>
		<load-on-startup>1</load-on-startup>
	</servlet>

	<servlet-mapping>
		<servlet-name>wafepa</servlet-name>
		<url-pattern>/</url-pattern>
	</servlet-mapping>
</web-app>

```

### CRUD operacije

* Dodati Servlet API i JSTL dependency-je u POM projekta (pom.xml)

```xml
<!-- Servlet API -->
<dependency>
	<groupId>javax.servlet</groupId>
	<artifactId>javax.servlet-api</artifactId>
	<version>3.1.0</version>
</dependency>

<!-- JSTL -->
<dependency>
	<groupId>javax.servlet</groupId>
	<artifactId>jstl</artifactId>
	<version>1.2</version>
</dependency>
```

* dodati context:component-scan za osnovni paket aplikacije u Spring context (application-context.xml)

```xml
    <context:component-scan base-package="jwts.wafepa" />
```

* Anotirati InMemoryActivityService kao @Service



* Napraviti ActivityController klasu u paketu jwts.wafepa.web.controller
* Injektovati ActivityService u ActivityController korišćenjem @Autowired anotacije
* Definisati mapiranja za HTTP zahteve za CRUD operacije (@RequestMapping)
* Konfigurisati Spring MVC da koristi anotacije (dodati <mvc:annotation-driven/> u Spring context)

```xml
	<mvc:annotation-driven/>
```

* Dodati view-ove: view za pregled svih aktivnosti i brisanje aktivnosti iz te liste, kao i view za dodavanje/izmenu zasebnih aktivnosti.
Kako bi mogli koristiti JSP i JSTL tagove, na početku JSP stranica importovati ove tagove:

```xml
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
```

* Dopuniti metode u ActivityController-u da vrše odgovarajuće CRUD operacije i da prosleđuju model odgovarajućim view-ovima

### Domaći zadatak
Po uzoru na aktivnosti, dodati mogućnost evidencije korisnika:

1. Dodati model klasu User (id, email, password, first name, last name)
2. Dodati servisni sloj za korisnike (interfejs i in-memory implementacija), kao i testove za servisni sloj
3. Dodati zaseban kontroler za korisnike
4. Napraviti zasebne view-ove za evidenciju korisnika