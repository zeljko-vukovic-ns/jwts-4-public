package jwts.wafapa.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jwts.wafapa.model.Activity;
import jwts.wafapa.service.ActivityService;

public class InMemoryActivityService implements ActivityService {
	private Map<Long, Activity> activities = new HashMap<>();
	private long nextId = 0;
	
	@Override
	public Activity findOne(Long id) {
		return activities.get(id);
	}

	@Override
	public List<Activity> findAll() {
		List<Activity> ret = new ArrayList<>();
		for (Activity activity : activities.values()) {
			ret.add(activity);
		}
		return ret;
	}

	@Override
	public Activity save(Activity activity) {
		if(activity.getId()==null){
			activity.setId(nextId++);
		}
		return activities.put(activity.getId(), activity);
	}

	@Override
	public Activity remove(Long id) {
		return activities.remove(id);
	}

}
