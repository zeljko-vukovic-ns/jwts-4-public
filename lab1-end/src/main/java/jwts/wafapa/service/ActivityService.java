package jwts.wafapa.service;

import java.util.List;

import jwts.wafapa.model.Activity;

public interface ActivityService {
	Activity findOne(Long id);
	List<Activity> findAll();
	Activity save(Activity activity);
	Activity remove(Long id);
}
