﻿## Lab 5 - ORM, Hibernate, Spring Data JPA

### ORM - Object-Relational Mapping

ORM predstavlja tehniku programiranja gde se objektni model korišćen u aplikaciji
mapira na relacioni model podataka u bazi podataka. Veoma korisna tehnika za
brz razvoj softvera (eng. rapid-software development), jer pravi
"virtuelnu objektnu bazu podataka" koja se može koristiti unutar izabranog programskog jezika.
Postoji veliki broj alata za ORM u modernim programskim jezicima, a jedan od
najpopularnijih je (Hibernate za Javu)[http://hibernate.org/orm/].

----


### Hibernate

Pored svog API-ja, Hibernate takođe predstavlja implementaciju 
Java Persistance API (JPA) specifikacije. Samim tim, može se lako koristiti
u bilo kom okruženju koje podržava JPA, kao što su Java SE aplikacije,
Java EE aplikativni serveri, itd.

Hibernate omogućava razvoj perzistentnog modela korišćenjem idioma
objektno-orijentisanog programiranja, uključujući nasleđivanje, polimorfizme,
asocijaciju, kompoziciju, itd. Hibernate ne zahteva nikakve posebne interfejse ili
klase za perzistentne klase i time omogućava da bilo koja klasa ili struktura
podataka bude perzistentna.

----

* Izvršiti sledeću skriptu nad MySQL bazom podataka:

```sql
DROP DATABASE IF EXISTS jwts3;
CREATE DATABASE jwts3 DEFAULT CHARACTER SET utf8;

USE jwts3;

GRANT ALL ON jwts3.* TO 'jwts3'@'%' IDENTIFIED BY 'jwts3';

FLUSH PRIVILEGES;
```

* Unutar src/main/resources napraviti direktorijum cfg,
 i u njemu napraviti fajl jdbc.properties (u ovom fajlu se definiše konekcija na bazu podataka):
 
```java
jdbc.driverClassName=com.mysql.jdbc.Driver
jdbc.dialect=org.hibernate.dialect.MySQLDialect
jdbc.databaseurl=jdbc:mysql://localhost:3306/jwts3
jdbc.username=jwts3
jdbc.password=jwts3
```

* Dodati potrebne dependency-je u pom.xml:

```xml
<!-- DB related dependencies -->
<dependency>
	<groupId>org.springframework.data</groupId>
	<artifactId>spring-data-jpa</artifactId>
	<version>1.4.5.RELEASE</version>
</dependency>
<dependency>
	<groupId>org.hibernate</groupId>
	<artifactId>hibernate-core</artifactId>
	<version>4.3.4.Final</version>
</dependency>
<dependency>
	<groupId>org.hibernate</groupId>
	<artifactId>hibernate-entitymanager</artifactId>
	<version>4.3.5.Final</version>
</dependency>
<dependency>
	<groupId>mysql</groupId>
	<artifactId>mysql-connector-java</artifactId>
	<version>5.1.30</version>
</dependency>
<dependency>
	<groupId>commons-dbcp</groupId>
	<artifactId>commons-dbcp</artifactId>
	<version>1.4</version>
</dependency>
<dependency>
	<groupId>org.springframework</groupId>
	<artifactId>spring-jdbc</artifactId>
	<version>4.0.1.RELEASE</version>
</dependency>
<dependency>
	<groupId>org.springframework</groupId>
	<artifactId>spring-orm</artifactId>
	<version>4.0.1.RELEASE</version>
</dependency>
<dependency>
	<groupId>org.springframework</groupId>
	<artifactId>spring-tx</artifactId>
	<version>4.0.1.RELEASE</version>
</dependency>
<dependency>
	<groupId>javax.transaction</groupId>
	<artifactId>javax.transaction-api</artifactId>
	<version>1.2</version>
</dependency>
```

* U application-context.xml zameniti postojeće zaglavlje sa opisom namespace-ova sa sledećim:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:mvc="http://www.springframework.org/schema/mvc"
    xmlns:p="http://www.springframework.org/schema/p"
    xmlns:context="http://www.springframework.org/schema/context"
    xmlns:util="http://www.springframework.org/schema/util"
    xmlns:jpa="http://www.springframework.org/schema/data/jpa"
   	xmlns:tx="http://www.springframework.org/schema/tx"
   	xmlns:jdbc="http://www.springframework.org/schema/jdbc" 
    xsi:schemaLocation="http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc-4.0.xsd
        http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.0.xsd
        http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util.xsd
        http://www.springframework.org/schema/data/jpa http://www.springframework.org/schema/data/jpa/spring-jpa.xsd
        http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd
        http://www.springframework.org/schema/jdbc http://www.springframework.org/schema/jdbc/spring-jdbc.xsd">
```
		
* U application-context.xml dodati konfiguraciju za konekciju na bazu podataka,
kao i konfiguraciju za Hibernate i JPA:

```xml
<context:property-placeholder location="classpath:cfg/properties/jdbc.properties"/>
	
<bean id="dataSource"
	class="org.apache.commons.dbcp.BasicDataSource" destroy-method="close"
	p:driverClassName="${jdbc.driverClassName}"
	p:url="${jdbc.databaseurl}" p:username="${jdbc.username}"
	p:password="${jdbc.password}" />
	
<bean id="entityManagerFactory" class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean">
	<property name="dataSource" ref="dataSource" />
	<property name="packagesToScan" value="jwts.wafepa.model" />
	<property name="jpaPropertyMap" ref="jpaPropertyMap" />
	<property name="jpaVendorAdapter">
		<bean class="org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter" />
</property>
</bean>
	
<util:map id="jpaPropertyMap">
	<entry key="hibernate.dialect" value="${jdbc.dialect}" />
	<entry key="hibernate.show_sql" value="true" />
	<entry key="hibernate.format_sql" value="true" />
	<entry key="hibernate.hbm2ddl.auto" value="update" />
	<entry key="hibernate.jdbc.batch_size" value="50" />
	
	<entry key="hibernate.connection.CharSet" value="utf8" />
	<entry key="hibernate.connection.characterEncoding" value="utf8" />
	<entry key="hibernate.connection.useUnicode" value="true" />
</util:map>
```

* Anotirati klasu Activity sa @Entity i @Table(name="tblActivity").

* Anotirati polje id klase Activity sa @Id, @GeneratedValue i @Column(name="id").

* Anotirati polje name klase Activity sa @Column(name="name").

* Pokrenuti aplikaciju i obratiti pažnju na promene u šemi baze podataka.

----

### Spring Data JPA

Spring Data JPA je deo velike familije projekata Spring Data
koji omogućava laku implementaciju repozitorijuma baziranih na JPA specifikaciji.
Sa Spring Data JPA je lakše praviti Spring aplikacije koje koriste tehnologije pristupa podacima.

Korišćenjem Spring Data JPA, developeri pišu svoje interfejse ka repozitorijumima,
zajedno sa "custom" metodama za pronalaženje podataka, dok Spring obezbeđuje automatsku implementaciju.

Prilikom pisanja "custom" metoda, potrebno je držati se
(specifikacije za davanje imena ovim metodama)[http://docs.spring.io/spring-data/jpa/docs/1.2.0.RELEASE/reference/html/#jpa.query-methods].

----

* U application-context.xml dodati konfiguraciju za Spring Data JPA 
i konfiguraciju transakcionog menadžera:

```xml
<jpa:repositories base-package="jwts.wafepa.repository" />
    
<tx:annotation-driven transaction-manager="transactionManager" />
<bean id="transactionManager" class="org.springframework.orm.jpa.JpaTransactionManager">
	<property name="entityManagerFactory" ref="entityManagerFactory" />
</bean>
```

* Napraviti paket jwts.wafepa.repository i u njemu napraviti interfejs ActivityRepository.
ActivityRepository treba da nasleđuje interfejs JpaRepository.
Anotirati interfejs ActivityRepository sa @Repository.

* U servisnom sloju dodati implementaciju interfejsa ActivityService koja će se zvati JpaActivityService.
Pomoću Dependency Injection mehanizma uključiti ActivityRepository u JpaActivityService (@Autowired) i 
implementirati metode servisa tako da koriste repozitorijum. Anotirati JpaActivityService sa @Transactional.

* Skloniti anotaciju @Service sa in-memory implementacije ActivityService-a, i dodati je na JPA implementaciju.

* Pokrenuti aplikaciju, uraditi CRUD operacije nad aktivnostima i posmatrati promene u bazi podataka.

----

#### "Custom" metode Spring Data JPA

* U ActivityRepository napraviti metodu List<Activity> findByName(String name).
Anotirati ovu metodu sa JPQL (Java Persistance Query Language)
upitom @Query("select a from Activity a where a.name = :name).
Anotirati parametar name sa @Param("name").

* Izmeniti ActivityController i view za pregled aktivnosti tako da podržavaju pretraživanje po imenu.

* Ukloniti anotaciju @Query sa metode findByName u ActivityRepository i testirati pretraživanje po imenu.

----

### Domaći zadatak

1. Po uzoru na aktivnosti, napraviti ORM mapiranje za korisnike (klasa User) pomoću Hibernate.
2. Dodati JPA implementaciju repozitorijuma za korisnike.
3. Dodati mogućnost pretraživanja korisnika po email-u.
