var wafepaApp = angular.module('wafepaApp', ['ngRoute']);

wafepaApp.controller('ActivitiesController', function($scope, $http) {
	
	$http.get('api/activities').success(function(data) {
		$scope.activities = data;
	});
});

// $routeProvider

wafepaApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
        templateUrl : 'resources/html/home.html'
	    })
	    .when('/activities', {
	        templateUrl : 'resources/html/activities.html',
	        controller : 'ActivitiesController'
	    })
	    .otherwise({
            redirectTo: '/'
        });
}]);